import psycopg2
import json
from sshtunnel import SSHTunnelForwarder

username = 'usrname_sso'
password = 'pass_sso'

print('START TUNNELING')
tunnel =  SSHTunnelForwarder(
    ('kawung.cs.ui.ac.id', 22),
    ssh_username=username,
    ssh_password=password,
    remote_bind_address=('dbpg.cs.ui.ac.id', 5432),
)


def select_query(sql):
    tunnel.start()
    print('TUNNEL STARTED')
    connect = psycopg2.connect(
        database='db2018035',
        user='db2018035',
        password='Be7ohKee',
        host='localhost',
        port=tunnel.local_bind_port
    )
    cur = connect.cursor()
    cur.execute('''
    SET search_path to bike_sharing;
    ''')

    cur.execute(sql)
    result = cur.fetchall()

    connect.close()
    tunnel.stop()

    return result

def insert_update_delete_query(sql):
    tunnel.start()
    print('TUNNEL STARTED')
    connect = psycopg2.connect(
        database='db2018035',
        user='db2018035',
        password='Be7ohKee',
        host='localhost',
        port=tunnel.local_bind_port
    )
    cur = connect.cursor()
    cur.execute('''
    SET search_path to bike_sharing;
    ''')
    cur.execute(sql)
    rows_count = cur.rowcount
    connect.commit()
    connect.close()
    tunnel.stop()
    return rows_count

