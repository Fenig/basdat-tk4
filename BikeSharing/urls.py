"""BikeSharing URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import include
from django.conf.urls.static import static
from django.conf import settings


urlpatterns = [
    path('', include('a0_landing_page.urls')),
    path('admin/', admin.site.urls),

    # A1
    path('login', include('a1_login.urls')),
    path('register', include('a1_register.urls')),
    path('topup', include('a1_top_up.urls')),

    # A2
    path('tasks/', include('a2_cud_tasks.urls')),
    path('events/', include('a2_cud_events.urls')),

    # A3
    path('bikes/', include('a3_view_bikes.urls')),
    path('stations/', include('a3_view_stations.urls')),

    # A4
    path('voucher/', include('a4_cud_vouchers.urls')),
    path('rents/', include('a4_cu_rents.urls')),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
