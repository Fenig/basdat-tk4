import json
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
from BikeSharing.database import select_query, insert_update_delete_query

def index(request):
    list_events = select_query("""
        SELECT *
        FROM ACARA;
    """)

    list_events = [{"id_event": v[0],
                    "judul": v[1],
                    "deskripsi": v[2],
                    "gratis": v[5],
                    "tanggal_mulai": v[3],
                    "tanggal_selesai": v[4]}
                    for v in list_events]
    
    events = {
        "events" : list_events,
        "account_name": request.COOKIES.get('username'),
        "role" : request.COOKIES.get('role')
    }

    return render(request, 'index-events.html', events)

@csrf_exempt
def create(request):
    data ={}
    if(request.method == 'GET'):
        list_stasiun = select_query("""
            SELECT *
            FROM STASIUN;
            """)

        list_stasiun = [{"id_stasiun": v[0],
                        "nama": v[4]}
                        for v in list_stasiun]

        data = {
            "event" : {
                "id_event":"1",
                "judul" : "Judul",
                "deskripsi":"Deskripsi",
                "gratis":"Ya/Tidak",
                "tanggal_mulai" : "Tanggal Mulai",
                "tanggal_selesai" : "Tanggal Selesai",
                "stasiun" : "Stasiun"
            },
            "account_name": request.COOKIES.get('username'),
            "role" : request.COOKIES.get('role'),
            "stasiun" : list_stasiun
        }

    elif(request.method == 'POST'):
        # TODO: implement query
        max_id = select_query("""
            SELECT MAX(CAST(id_acara AS DECIMAL))
            FROM ACARA;
            """)

        id = int(max_id[0][0]) + 1
        judul = request.POST.get("judul")
        deskripsi = request.POST.get("deskripsi")
        gratis = request.POST.get("gratis")
        tanggal_mulai = request.POST.get("tanggal_mulai")
        tanggal_selesai = request.POST.get("tanggal_selesai")
        stasiun = request.POST.getlist("stasiun")

        rows_insert_to_acara = insert_update_delete_query("""
            INSERT INTO ACARA
            VALUES ('%s', '%s', '%s', '%s', '%s', '%s');
            """ % (id, judul, deskripsi, tanggal_mulai, tanggal_selesai, gratis))

        for st_idx in stasiun:
            rows_insert_to_acara_stasiun = insert_update_delete_query("""
                INSERT INTO ACARA_STASIUN
                VALUES ('%s', '%s');
                """ % (st_idx, id))
        return redirect('/events/')
    return render(request, 'create-events.html', data)

@csrf_exempt
def update(request, id):
    data ={}
    if(request.method == 'GET'):
        curr_event = select_query("""
            SELECT *
            FROM ACARA
            WHERE id_acara = '%s';
            """ % (id))

        curr_event = [{"id_event": v[0],
                    "judul": v[1],
                    "deskripsi": v[2],
                    "gratis": v[5],
                    "tanggal_mulai": v[3],
                    "tanggal_selesai": v[4]}
                    for v in curr_event]

        list_stasiun = select_query("""
            SELECT *
            FROM STASIUN;
            """)

        list_stasiun_selected = select_query("""
            SELECT s.id_stasiun, s.nama
            FROM ACARA_STASIUN ast, STASIUN s
            WHERE ast.id_acara = '%s' AND ast.id_stasiun = s.id_stasiun;
            """ % (id))

        new_list_stasiun = []

        for a in list_stasiun:
            flag = False
            for b in list_stasiun_selected:
                if(a[0] == b[0]):
                    flag = True
            if(flag):
                new_list_stasiun.append({"id_stasiun" : a[0], "nama" : a[4], "selected" : True})
            else:
                new_list_stasiun.append({"id_stasiun" : a[0], "nama" : a[4], "selected" : False})

        data = {
            "event" : curr_event[0],
            "account_name": request.COOKIES.get('username'),
            "role" : request.COOKIES.get('role'),
            "stasiun" : new_list_stasiun
        }

    elif(request.method == 'POST'):
        # TODO: implement query
        judul = request.POST.get("judul")
        deskripsi = request.POST.get("deskripsi")
        gratis = request.POST.get("gratis")
        tanggal_mulai = request.POST.get("tanggal_mulai")
        tanggal_selesai = request.POST.get("tanggal_selesai")
        stasiun = request.POST.getlist("stasiun")

        rows_updated_to_acara = insert_update_delete_query("""
            UPDATE ACARA
            SET judul = '%s', deskripsi = '%s', is_free = '%s', tgl_mulai = '%s', tgl_akhir = '%s'
            WHERE id_acara = '%s';
            """ % (judul, deskripsi, gratis, tanggal_mulai, tanggal_selesai, id))

        rows_deleted_to_acara_stasiun = insert_update_delete_query("""
                DELETE FROM ACARA_STASIUN
                WHERE id_acara = '%s';
                """ % (id))
        
        for st_idx in stasiun:
            rows_insert_to_acara_stasiun = insert_update_delete_query("""
                INSERT INTO ACARA_STASIUN
                VALUES ('%s', '%s');
                """ % (st_idx, id))
            
        return redirect('/events/')
    return render(request, 'update-events.html', data)

@csrf_exempt
def delete(request, id):
    rows_deleted_to_acara_stasiun = insert_update_delete_query("""
        DELETE FROM ACARA_STASIUN
        WHERE id_acara = '%s';
        """ % (id))
    rows_deleted_to_acara = insert_update_delete_query("""
        DELETE FROM ACARA
        WHERE id_acara = '%s';
        """ % (id))
    return redirect('/events/')