from django.urls import path

from . import views

app_name = 'event'
urlpatterns = [
    path('', views.index, name='events-index'),
    path('create/', views.create, name='create'),
    path('update/<int:id>', views.update, name='update'),
    path('delete/<int:id>', views.delete, name='delete')
]
