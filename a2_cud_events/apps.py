from django.apps import AppConfig


class A2CudEventsConfig(AppConfig):
    name = 'a2_cud_events'
