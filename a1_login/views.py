import json
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
from BikeSharing.database import select_query, insert_update_delete_query

@csrf_exempt
def index(request):
    response = render(request, 'login/login.html', 
        { "account_name": request.COOKIES.get('username'),
        "role" : request.COOKIES.get('role')})
    if(request.method == 'POST'):
        ktp = request.POST.get("ktp")
        email = request.POST.get("email")

        admin = select_query("""
            SELECT p.nama
            FROM person p, petugas a WHERE 
            p.email = '%s' AND p.ktp = '%s' AND a.ktp=p.ktp;
            """ % (email, ktp))
        
        username = ""
        no_kartu_user = ""
        role = ""
        if (admin == []):
            user = select_query("""
            SELECT p.nama, a.no_kartu
            FROM person p, anggota a WHERE 
            p.email = '%s' AND p.ktp = '%s' AND a.ktp=p.ktp;
            """ % (email, ktp))

            if(user != []):
                username = user[0][0]
                role = "anggota"
                no_kartu_user = user[0][1]
                response.set_cookie('username', username) 
                response.set_cookie('role', role) 
                response.set_cookie('no_kartu_user', no_kartu_user)
        else:
            role = "admin"
            username = admin[0][0]
            response.set_cookie('username', username) 
            response.set_cookie('role', role) 
            response.set_cookie('no_kartu_user', no_kartu_user)
        
        
    return response
