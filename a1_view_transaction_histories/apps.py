from django.apps import AppConfig


class A1TransactionHistoryConfig(AppConfig):
    name = 'a1_transaction_history'
