from django.urls import path

from . import views

app_name = 'bikes'
urlpatterns = [
    path('', views.index, name='bikes-index'),
    path('update/', views.update, name='update'),
    path('delete/', views.delete, name='delete')
]
