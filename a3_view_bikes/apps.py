from django.apps import AppConfig


class A3BikeListConfig(AppConfig):
    name = 'a3_bike_list'
