from django.shortcuts import render


def index(request):
    context = {
        "account_name": "Akhmad Ramadhan Yamin",
        "role": "admin",
        "account_balance": "Rp5000,-",
        "bikes": [
            {
                "nomor": "13u78q",
                "merk": "BMX",
                "jenis": "Dewasa",
                "stasiun": "Stasiun Basdat Depok 1",
                "status": "Tersedia",
                "penyumbang": "-",
            },
        ],
    }
    # TODO: Implement uwu
    return render(request, 'view_bikes/view_bikes.html', context)

def update(request):
    data ={}
    if(request.method == 'GET'):
        data = {
            "account_name": "Akhmad Ramadhan Yamin",
            "role": "admin",
            "bike" : {
                "nomor": "13u78q",
                "merk": "BMX",
                "jenis": "Dewasa",
                "stasiun": "Stasiun Basdat Depok 1",
                "status": "Tersedia",
                "penyumbang": "-",
            }
        }
    elif(request.method == 'POST'):
        # TODO: implement query
        data = {
            "account_name": "Akhmad Ramadhan Yamin",
            "role": "admin",
            "bike" : {
                "nomor": "13u78q",
                "merk": "BMX",
                "jenis": "Dewasa",
                "stasiun": "Stasiun Basdat Depok 1",
                "status": "Tersedia",
                "penyumbang": "-",
            }
        }
        return redirect('/bikes/')
    return render(request, 'update-bikes.html', data)

def delete(request):
    data ={}
    if(request.method == 'GET'):
        data = {
            "account_name": "Akhmad Ramadhan Yamin",
            "role": "admin",
            "bike" : {
                "nomor": "13u78q",
                "merk": "BMX",
                "jenis": "Dewasa",
                "stasiun": "Stasiun Basdat Depok 1",
                "status": "Tersedia",
                "penyumbang": "-",
            }
        }
    elif(request.method == 'POST'):
        # TODO: implement query
        data = {
            "account_name": "Akhmad Ramadhan Yamin",
            "role": "admin",
            "bike" : {
                "nomor": "13u78q",
                "merk": "BMX",
                "jenis": "Dewasa",
                "stasiun": "Stasiun Basdat Depok 1",
                "status": "Tersedia",
                "penyumbang": "-",
            }
        }
        return redirect('/bikes/')
    return render(request, 'delete-bikes.html', data)