from django.shortcuts import render


def index(request):
    context = {
        "account_name": "Akhmad Ramadhan Yamin",
        "role": "admin",
        "stations": [
            {
                "name": "Stasiun Jalan Kenanga",
                "coord": "-3.14159285, 115.213134",
                "addr": "Jl. Temen Oblak, Paraguay, Lorem ipsum dolor sit amet.",
                "bike_amt": 9,
            },
            {
                "name": "Stasiun Jalan Salam",
                "coord": "-3.177013, 115.000000",
                "addr": "Jl. Waloyo Yamoni, Kab. Baba Yetu, Lorem ipsum dolor sit amet.",
                "bike_amt": 0,
            },
            {
                "name": "Stasiun Jalan Agathis",
                "coord": "-3.14159285, 115.213134",
                "addr": "Mt. Vinson Massif, Antarctica, Lorem ipsum dolor sit amet.",
                "bike_amt": 9,
            },
            {
                "name": "Stasiun Ada Aja",
                "coord": "-3.14159285, 115.213134",
                "addr": "Mt. Vinson Massif, Antarctica, Lorem ipsum dolor sit amet.",
                "bike_amt": 9,
            },
        ],
    }
    return render(request, 'view_stations/view_stations.html', context)

def update(request):
    data ={}
    if(request.method == 'GET'):
        data = {
            "account_name": "Akhmad Ramadhan Yamin",
            "role": "admin",
            "station" : {
                "name": "Stasiun Jalan Kenanga",
                "coord": "-3.14159285, 115.213134",
                "addr": "Jl. Temen Oblak, Paraguay, Lorem ipsum dolor sit amet.",
                "bike_amt": 9,
            }
        }
    elif(request.method == 'POST'):
        # TODO: implement query
        data = {
            "account_name": "Akhmad Ramadhan Yamin",
            "role": "admin",
            "station" : {
                "name": "Stasiun Jalan Kenanga",
                "coord": "-3.14159285, 115.213134",
                "addr": "Jl. Temen Oblak, Paraguay, Lorem ipsum dolor sit amet.",
                "bike_amt": 9,
            }
        }
        return redirect('/stations/')
    return render(request, 'update-stations.html', data)

def delete(request):
    data ={}
    if(request.method == 'GET'):
        data = {
            "account_name": "Akhmad Ramadhan Yamin",
            "role": "admin",
            "station" : {
                "name": "Stasiun Jalan Kenanga",
                "coord": "-3.14159285, 115.213134",
                "addr": "Jl. Temen Oblak, Paraguay, Lorem ipsum dolor sit amet.",
                "bike_amt": 9,
            }
        }
    elif(request.method == 'POST'):
        # TODO: implement query
        data = {
            "account_name": "Akhmad Ramadhan Yamin",
            "role": "admin",
            "station" : {
                "name": "Stasiun Jalan Kenanga",
                "coord": "-3.14159285, 115.213134",
                "addr": "Jl. Temen Oblak, Paraguay, Lorem ipsum dolor sit amet.",
                "bike_amt": 9,
            }
        }
        return redirect('/stations/')
    return render(request, 'delete-stations.html', data)