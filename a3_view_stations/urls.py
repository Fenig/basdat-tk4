from django.urls import path

from . import views

app_name = 'stations'
urlpatterns = [
    path('', views.index, name='stations-index'),
    path('update/', views.update, name='update'),
    path('delete/', views.delete, name='delete')
]
