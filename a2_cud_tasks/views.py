import json
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
from BikeSharing.database import select_query, insert_update_delete_query

def index(request):
    list_tasks = select_query("""
        SELECT pn.ktp, ps.nama as nama_petugas, pn.start_datetime, pn.end_datetime, pn.id_stasiun, st.nama as nama_stasiun
        FROM penugasan pn, person ps, petugas pt, stasiun st
        WHERE pn.ktp = pt.ktp AND pt.ktp = ps.ktp AND st.id_stasiun = pn.id_stasiun;
    """)

    list_tasks = [{"id_task": v + 1,
                    "no_ktp": list_tasks[v][0],
                    "nama_petugas": list_tasks[v][1],
                    "tanggal_mulai": list_tasks[v][2],
                    "tanggal_selesai": list_tasks[v][3],
                    "id_stasiun": list_tasks[v][4],
                    "nama_stasiun": list_tasks[v][5]}
                    for v in range(len(list_tasks))]

    tasks = {
        "tasks" : list_tasks,
        "account_name": request.COOKIES.get('username'),
        "role" : request.COOKIES.get('role')
    }
    return render(request, 'index-tasks.html', tasks)

@csrf_exempt
def create(request):
    data ={}
    if(request.method == 'GET'):
        list_stasiun = select_query("""
            SELECT *
            FROM STASIUN;
            """)

        list_stasiun = [{"id_stasiun": v[0],
                        "nama": v[4]}
                        for v in list_stasiun]

        list_petugas = select_query("""
            SELECT pt.ktp, ps.nama
            FROM PETUGAS pt, PERSON ps
            WHERE pt.ktp = ps.ktp;
            """)

        list_petugas = [{"no_ktp": v[0],
                        "nama_petugas": v[1]}
                        for v in list_petugas]
        
        data = {
            "task" : {
                "id_task":"1",
                "petugas" : "Petugas",
                "tanggal_mulai" : "Tanggal Mulai",
                "tanggal_selesai" : "Tanggal Selesai",
                "stasiun" : "Stasiun"
            },
            "account_name": request.COOKIES.get('username'),
            "role" : request.COOKIES.get('role'),
            "stasiun" : list_stasiun,
            "petugas" : list_petugas
        }
    elif(request.method == 'POST'):
        # TODO: implement query
        no_ktp = request.POST.get("petugas")
        tanggal_mulai = request.POST.get("tanggal_mulai")
        tanggal_selesai = request.POST.get("tanggal_selesai")
        stasiun = request.POST.get("stasiun")

        rows_insert_to_acara = insert_update_delete_query("""
            INSERT INTO PENUGASAN
            VALUES ('%s', '%s', '%s', '%s');
            """ % (no_ktp, tanggal_mulai, stasiun, tanggal_selesai))
        return redirect('/tasks/')
    return render(request, 'create-tasks.html', data)

@csrf_exempt
def update(request, ktp, tgl_mulai, id_stasiun):
    data ={}
    if(request.method == 'GET'):
        curr_task = select_query("""
            SELECT pn.ktp, ps.nama as nama_petugas, pn.start_datetime, pn.end_datetime, pn.id_stasiun, st.nama as nama_stasiun
            FROM penugasan pn, person ps, petugas pt, stasiun st
            WHERE pn.ktp = pt.ktp AND pt.ktp = ps.ktp AND st.id_stasiun = pn.id_stasiun AND pn.ktp = '%s' AND pn.start_datetime = '%s' AND pn.id_stasiun = '%s';
            """ % (ktp, tgl_mulai, id_stasiun))

        curr_task = [{"no_ktp": curr_task[v][0],
                    "nama_petugas": curr_task[v][1],
                    "tanggal_mulai": curr_task[v][2],
                    "tanggal_selesai": curr_task[v][3],
                    "id_stasiun": curr_task[v][4],
                    "nama_stasiun": curr_task[v][5]}
                    for v in range(len(curr_task))]
        
        list_stasiun = select_query("""
            SELECT *
            FROM STASIUN;
            """)

        list_stasiun = [{"id_stasiun": v[0],
                        "nama": v[4]}
                        for v in list_stasiun]

        list_petugas = select_query("""
            SELECT pt.ktp, ps.nama
            FROM PETUGAS pt, PERSON ps
            WHERE pt.ktp = ps.ktp;
            """)

        list_petugas = [{"no_ktp": v[0],
                        "nama_petugas": v[1]}
                        for v in list_petugas]

        data = {
            "task" : curr_task[0],
            "account_name": request.COOKIES.get('username'),
            "role" : request.COOKIES.get('role'),
            "stasiun" : list_stasiun,
            "petugas" : list_petugas
        }
    elif(request.method == 'POST'):
        # TODO: implement query
        no_ktp = request.POST.get("petugas")
        tanggal_mulai = request.POST.get("tanggal_mulai")
        tanggal_selesai = request.POST.get("tanggal_selesai")
        stasiun = request.POST.get("stasiun")

        rows_updated_to_acara = insert_update_delete_query("""
            UPDATE PENUGASAN
            SET ktp = '%s', start_datetime = '%s', end_datetime = '%s', id_stasiun = '%s'
            WHERE ktp = '%s' AND start_datetime = '%s' AND id_stasiun = '%s';
            """ % (no_ktp, tanggal_mulai, tanggal_selesai, stasiun, ktp, tgl_mulai, id_stasiun))
        
        return redirect('/tasks/')
    return render(request, 'update-tasks.html', data)

@csrf_exempt
def delete(request, ktp, tgl_mulai, id_stasiun):
    rows_deleted_to_acara_stasiun = insert_update_delete_query("""
        DELETE FROM PENUGASAN
        WHERE ktp = '%s' AND start_datetime = '%s' AND id_stasiun = '%s';
        """ % (ktp, tgl_mulai, id_stasiun))
    return redirect('/tasks/')