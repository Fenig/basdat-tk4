from django.apps import AppConfig


class A2CudTaskConfig(AppConfig):
    name = 'a2_cud_task'
