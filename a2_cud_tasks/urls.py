from django.urls import path

from . import views

app_name = 'task'
urlpatterns = [
    path('', views.index, name='tasks-index'),
    path('create/', views.create, name='create'),
    path('update/<str:ktp>/<str:tgl_mulai>/<str:id_stasiun>', views.update, name='update'),
    path('delete/<str:ktp>/<str:tgl_mulai>/<str:id_stasiun>', views.delete, name='delete')
]
