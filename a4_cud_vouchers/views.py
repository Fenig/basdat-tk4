import json
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
from BikeSharing.database import select_query, insert_update_delete_query

def getData(idx):
    v = select_query("""
        SELECT *
        FROM VOUCHER WHERE
        id_voucher = '%s';
    """ % idx)
    v = v[0]
    try:
        claimer = select_query("""
        SELECT p.nama
        FROM person p, anggota a WHERE 
        a.no_kartu = '%s' AND
        a.ktp = p.ktp;
        """ % v[5])
        claimer =claimer[0][0]
    except:
        claimer = ""

    data = {"id_voucher": v[0],
            "nama": v[1],
            "kategori": v[2],
            "nilai_poin": v[3],
            "deskripsi": v[4],
            "nama_claimer": claimer}
    return data
    
def index(request):

    try:
        role = request.COOKIES.get('role')
        name = request.COOKIES.get('username')
    except:
        return redirect('/login/')

    query_result = select_query("""
        SELECT *
        FROM VOUCHER;
    """)

    list_vouchers = []

    for v in query_result:
        try:
            claimer = select_query("""
            SELECT p.nama
            FROM person p, anggota a WHERE 
            a.no_kartu = '%s' AND
            a.ktp = p.ktp;
            """ % v[5])
            claimer =claimer[0][0]
        except:
            claimer = ""

        voucher = {"id_voucher": v[0],
                    "nama": v[1],
                    "kategori": v[2],
                    "nilai_poin": v[3],
                    "deskripsi": v[4],
                    "nama_claimer": claimer}
        list_vouchers.append(voucher)

    vouchers = {
        "vouchers" : list_vouchers,
        "account_name" : name,
        "role" : role
    }

    return render(request, 'voucher-index.html', vouchers)
    
@csrf_exempt
def create(request):
    try:
        role = request.COOKIES.get('role')
    except:
        return redirect('/login/')

    data ={}
    if(request.method == 'GET'):
        data = {
            "voucher" : {
                "nama" : "Nama Voucher",
                "kategori" : "Kategori Voucher",
                "nilai_poin" : "Nilai Poin Voucher",
                "deskripsi" : "Deskripsi Voucher",
                "jumlah" : "Jumlah Voucher"
            },
            "role" : role
        }
    elif(request.method == 'POST'):
        max_id = select_query("""
            SELECT MAX(CAST(id_voucher AS DECIMAL))
            FROM VOUCHER;
            """)

        id = int(max_id[0][0]) + 1
        nama = request.POST.get("nama")
        kategori = request.POST.get("kategori")
        nilai_poin = request.POST.get("nilai_poin")
        deskripsi = request.POST.get("deskripsi")

        inserted = insert_update_delete_query("""
            INSERT INTO VOUCHER
            VALUES ('%s', '%s', '%s', '%s', '%s', NULL);
            """ % (id, nama, kategori, nilai_poin, deskripsi))
        return redirect('/voucher/')
    return render(request, 'create-voucher.html', data)

@csrf_exempt
def update(request, id):
    try:
        role = request.COOKIES.get('role')
    except:
        return redirect('/login/')

    data ={}
    if(request.method == 'GET'):
        data = {
            "voucher" : getData(id),
            "role" : role
        }
    elif(request.method == 'POST'):

        # TODO: implement query
        nama = request.POST.get("nama")
        kategori = request.POST.get("kategori")
        nilai_poin = request.POST.get("nilai_poin")
        deskripsi = request.POST.get("deskripsi")

        updated = insert_update_delete_query("""
            UPDATE VOUCHER
            SET nama = '%s', kategori = '%s', nilai_poin = '%s', deskripsi = '%s' 
            WHERE id_voucher = '%s';
            """ % (nama, kategori, nilai_poin, deskripsi, id)) 
        return redirect('/voucher/')
    return render(request, 'update-voucher.html', data)

@csrf_exempt
def delete(request,id):
    try:
        role = request.COOKIES.get('role')
    except:
        return redirect('/login/') 

    data ={}
    if(request.method == 'GET'):
        data = {
            "voucher" : getData(id),
            "role" : role
        }
    elif(request.method == 'POST'):
        deleted = insert_update_delete_query("""
        DELETE FROM VOUCHER
        WHERE id_voucher = '%s';
        """ % (id))
        return redirect('/voucher/')
    return render(request, 'delete-voucher.html', data)

@csrf_exempt
def claim(request, id): 
    try:
        role = request.COOKIES.get('role')
        no_kartu_user = request.COOKIES.get('nomor_kartu_user')
    except:
        return redirect('/login/')

    data ={}
    if(request.method == 'GET'):
        data = {
            "voucher" : getData(id),
            "role" : role
        }
    elif(request.method == 'POST'):
        print("masuk post")
        try:
            
            updated = insert_update_delete_query("""
            UPDATE VOUCHER
            SET no_kartu_anggota='%s' 
            WHERE id_voucher = '%s';
            """ % (no_kartu_user, id))
            return render(request, 'claim-voucher-success.html', data)
        except:
            return redirect('/voucher/')

        
    return render(request, 'claim-voucher.html', data)

def voucher_claimed(request):
    return render(request, 'claim-voucher-success.html', {})
