from django.urls import path

from . import views

app_name = 'voucher'
urlpatterns = [
    path('', views.index, name='voucher-index'),
    path('create/', views.create, name='v-create'),
    path('update/<int:id>', views.update, name='v-update'),
    path('delete/<int:id>', views.delete, name='v-delete'),
    path('claim/<int:id>', views.claim, name='v-claim'),
    path('claim-success/', views.voucher_claimed, name='v-claim-success'),
]
