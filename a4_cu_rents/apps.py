from django.apps import AppConfig


class A4CuRentConfig(AppConfig):
    name = 'a4_cu_rent'
