import json, datetime
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
from BikeSharing.database import select_query, insert_update_delete_query


def getBikes():
    res = select_query("""
            SELECT *
            FROM SEPEDA;
        """)
    bikes = []
    for v in res:
        if (v[3]): #asumsi: t = tersedia
            bike = {
                "nomor" : v[0],
                "id_stasiun" : v[4]
            }
            bikes.append(bike)
    return bikes

def get_time():
    t = datetime.datetime.now()
    s = t.strftime('%Y-%m-%d %H:%M:%S.%f')
    return s[:-7]

# Create your views here.
def index(request):
    try:
        role = request.COOKIES.get('role')
        name = request.COOKIES.get('username') 
        no_kartu_anggota = request.COOKIES.get('nomor_kartu_user')
    except:
        return redirect('/login/')

    peminjaman_list = []
    query_result = ""
    if role == "admin":
        query_result = select_query("""
            SELECT *
            FROM PEMINJAMAN;
            """)
    elif role == "anggota":
        query_result = select_query("""
            SELECT *
            FROM PEMINJAMAN WHERE
            no_kartu_anggota = '%s';
        """ % no_kartu_anggota)

    for v in query_result:
        rent = {"no_kartu_anggota" : v[0],
            "datetime_pinjam" : str(v[1]),
            "datetime_kembali" : str(v[4]),
            "nomor_sepeda" : v[2],
            "id_stasiun" : v[3],
            "biaya" : v[5],
            "denda" : v[6]
            }
        peminjaman_list.append(rent)
    result = {
            "rents" : peminjaman_list,
            "account_name" : name,
            "role" : role
        }
    return render(request, 'rents-index.html', result)

@csrf_exempt
def create(request):
    try:
        role = request.COOKIES.get('role')
        no_kartu_anggota = request.COOKIES.get('nomor_kartu_user')
    except:
        return redirect('/login/')

    data = {}
    if(request.method == "GET"):
        data = {
            "bikes" : getBikes(),
            "role" : role
        }
    elif(request.method == "POST"):
        tmp = request.POST.get("bike").split(".")
        id_stasiun = tmp[0]
        no_sepeda = tmp[1]

        inserted = insert_update_delete_query("""
            INSERT INTO PEMINJAMAN
            VALUES ('%s', '%s', '%s', '%s', NULL, '0', '0');
            """ % (no_kartu_anggota, str(get_time()), no_sepeda, id_stasiun))

        updated_bike = insert_update_delete_query("""
            UPDATE SEPEDA
            SET status = 'f' 
            WHERE nomor='%s';
            """ % (no_sepeda))

        return redirect('/rents/')
    return render(request, 'create-rents.html', data)

@csrf_exempt
def update(request):
    if(request.method == "POST"):
        no_kartu_anggota = request.POST.get("no_kartu_anggota")
        datetime_pinjam = request.POST.get("datetime_pinjam")
        nomor_sepeda = request.POST.get("nomor_sepeda")
        id_stasiun = request.POST.get("id_stasiun")

        updated = insert_update_delete_query("""
            UPDATE PEMINJAMAN
            SET datetime_kembali = '%s' 
            WHERE no_kartu_anggota = '%s' AND datetime_pinjam = '%s' AND nomor_sepeda = '%s' AND id_stasiun = '%s';
            """ % (str(get_time()),no_kartu_anggota, datetime_pinjam, nomor_sepeda, id_stasiun)) 

        updated_bike = insert_update_delete_query("""
            UPDATE SEPEDA
            SET status = 't' 
            WHERE nomor='%s';
            """ % (nomor_sepeda)) 
        
    return redirect('/rents/')