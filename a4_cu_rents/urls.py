from django.urls import path

from . import views

app_name = 'rents'
urlpatterns = [
    path('', views.index, name='rents-index'),
    path('update/', views.update, name='update'),
    path('create/', views.create, name='create'),
]
