from django.urls import path

from . import views

app_name = 'view_stations'
urlpatterns = [
    path('', views.index, name='index'),
]
